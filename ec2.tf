resource "aws_instance" "docker-server" {

  ami           = "ami-09040d770ffe2224f"
  instance_type = "t2.micro"
  key_name      = "avanti"
  security_groups = [ "allow_ssh", "allow_http", "allow_egress" ]
  user_data = file("script.sh")
  
  tags = {
    Name = "Docker"
  }
}